
#如何得到更多从你的队友中
去得到更多从你的队友中，创建一个好的团队精神和努力使每个人都有个人挑战和亲自参与其中。
为了创建团队精神，老掉牙的东西像logoized衣服和同行者是好的，但不如个人的尊重那么好。如果每个人相互尊重，是没有人会让任何人失望的。团队精神的创建就是人们在考虑自己的利益之前考虑团队的利益并为之作出牺牲。作为一个领导者，你不能要求太多超过你给予你自己的在这个方面。
团队领导的关键之一是促进共识，这样每个人都会支持。这有时就意味着就允许你的队友是错误的。换言之，如果它对项目损伤的不多，你就可以让你的团队用他们自己方式做事情，在共识的基础上，即时你有很大的自信相信那样做是错的，当遇到这种情况，不要同意，简单的公开不同意并接受共识。不要听起来像是有损伤，或者你像是被迫的，简单的状态就是你不同意但认为团队的共识更重要。这往往会导致他们去回溯。不要坚持他们实行他们最初的计划如果他们做回溯。
如果有个人不同意你讨论了来自所有适当方面后的问题，简单断言就是你必须做一个决定和你的决定是什么。
询问你的团队，全部依照小组和个人，他们想创造怎样的团队精神和什么有助于一个高效的团队。
要经常的去赞美。特别赞美那些不同意你时值得称赞的人。在公共场合表扬，私下批评；有一个例子：有时候增长或纠正错误不能被表扬而是避免引起尴尬的关注由于原始的错误。因此增长（错误）应被私下表扬。
Next [How to Divide Problems Up](04-How to Divide Problems Up.md)